﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace TG.Exam.SQL
{
    public class DAL
    {
        private SqlConnection GetConnection() 
        {
            var connectionString = ConfigurationManager.AppSettings["ConnectionString"];

            var con = new SqlConnection(connectionString);

            con.Open();

            return con;
        }

        private DataSet GetData(string sql)
        {
            var ds = new DataSet();

            using (var con = GetConnection())
            {
                using (var cmd = new SqlCommand(sql, con))
                {
                    using (var adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(ds);
                    }
                }
            }

            return ds;
        }

        private void Execute(string sql)
        {
            using (var con = GetConnection())
            {
                using (var cmd = new SqlCommand(sql, con))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public DataTable GetAllOrders()
        {
            var sql = @"
SELECT 
    OrderId,
    OrderDate 
FROM [Orders]
ORDER BY OrderDate DESC
";

            var ds = GetData(sql);

            var result = ds.Tables.OfType<DataTable>().FirstOrDefault();

            return result;
        }
        public DataTable GetAllOrdersWithCustomers()
        {
            var sql = @"
SELECT 
    o.OrderId,
    o.OrderDate,
    RTRIM(c.CustomerFirstName) + ' ' + LTRIM(RTRIM(c.CustomerLastName)) AS Customer
FROM Orders o
JOIN Customers c ON c.CustomerId = o.OrderCustomerId
ORDER BY o.OrderDate DESC
";

            var ds = GetData(sql);

            var result = ds.Tables.OfType<DataTable>().FirstOrDefault();

            return result;
        }

        public DataTable GetAllOrdersWithPriceUnder(int price)
        {
            var sql = string.Format(@"
SELECT 
    o.OrderId,
    OrderDate,
    SUM(oi.[Count] * i.ItemPrice) AS OrderTotal
FROM 
    Orders o
JOIN OrdersItems oi ON oi.OrderId = o.OrderId
JOIN Items i ON i.ItemId = oi.ItemId
GROUP BY 
    o.OrderId,
    o.OrderDate
HAVING SUM(oi.[Count] * i.ItemPrice) > {0}
ORDER BY 
    o.OrderDate DESC", price);

            var ds = GetData(sql);

            var result = ds.Tables.OfType<DataTable>().FirstOrDefault();

            return result;
        }

        public void DeleteCustomer(int orderId)
        {
            var sql = string.Format(@"
DELETE c 
FROM Customers c
JOIN Orders o ON o.OrderCustomerId = c.CustomerId
WHERE o.OrderId = {0}
", orderId);

            Execute(sql);
        }

        internal DataTable GetAllItemsAndTheirOrdersCountIncludingTheItemsWithoutOrders()
        {
            var sql = @"
SELECT 
    i.ItemId,
    i.ItemName,
    ISNULL(COUNT(DISTINCT oi.OrderId),0) AS Count
FROM Items i
LEFT JOIN OrdersItems oi ON oi.ItemId = i.ItemId
GROUP BY  
    i.ItemId, 
    i.ItemName
";

            var ds = GetData(sql);

            var result = ds.Tables.OfType<DataTable>().FirstOrDefault();

            return result;
        }
    }
}
