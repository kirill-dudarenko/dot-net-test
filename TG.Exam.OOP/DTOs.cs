﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TG.Exam.OOP
{
    public class DTOBase
    {
        public virtual string ToString2()
        {
            return this.GetType().Name.PadRight(25);
        }
    }

    public class Employee : DTOBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Salary { get; set; }
        
        public override string ToString2()
        {
            return base.ToString2() + string.Format("FN:{0} LN:{1} S:{2} ", FirstName, LastName, Salary);
        }
    }

    public class SalesManager : Employee
    {
        public int BonusPerSale { get; set; }
        public int SalesThisMonth { get; set; }

        public override string ToString2()
        {
            return base.ToString2() + string.Format("BPS:{0} STM:{1} ", BonusPerSale, SalesThisMonth);
        }
    }

    public class CustomerServiceAgent : Employee
    {
        public int Customers { get; set; }

        public override string ToString2()
        {
            return base.ToString2() + string.Format("C:{0} ", Customers);
        }
    }

    public class Dog:DTOBase
    {
        public string Name { get; set; }
        public int Age { get; set; }

        public override string ToString2()
        {
            return base.ToString2() + string.Format("N:{0} A:{1} ", Name, Age);
        }
    }
}
