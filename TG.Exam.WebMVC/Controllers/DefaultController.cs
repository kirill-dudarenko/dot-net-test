﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Salestech.Exam.WebMVC.Controllers
{
    public class DefaultController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Index()
        {
            var users = TG.Exam.WebMVC.Models.User.GetAll();
            users.ForEach(x => x.Age = x.Age + 10);
            
            return Ok(users);
        }
    }
}
