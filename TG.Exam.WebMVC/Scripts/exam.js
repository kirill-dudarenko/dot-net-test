﻿
$(document).ready(function () {
    $('#btnLoadUsersAsync').on('click', function () {
        var Url = "/api/Default/Index";

        var options = {
            url: Url,
            type: "GET",
            data: {}
        };

        $.ajax(options).done(function (data) {
            var $table = $('#table-body');
            $table.empty();
            
            for (i in data)
            {
                var $user = data[i];
                $table.append(
                    "<tr>" +
                    "<td>" + $user.FirstName + "</td>" +
                    "<td>" + $user.LastName + "</td>" +
                    "<td>" + $user.Age + "</td>" +
                    "<td>" + "Async" + "</td>" +
                    "</tr>");
            };
        });
        return false;
    });
});
