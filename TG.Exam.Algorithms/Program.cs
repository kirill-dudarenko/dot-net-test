﻿using System;

namespace TG.Exam.Algorithms
{
    class Program
    {
        /* this method accumulates sum of 2 variables to the last of sum items. 
         * Advantages
         *      - speed as the algorithm is recursive.
         *      - memory usage is optimized as only 2 variables are used instead of 3.
         * Disadvantages
         *      - recursive algorithms adds complexity, reading and debugging might be slown down.
         *      - StackOverflow exception will be thrown in case of call below
         *        Console.WriteLine("Foo result: {0}", Foo(7, 2, int.MaxValue));                   
         */
        static int Foo(int a, int b, int c)
        {
            if (1 < c)
                return Foo(b, b + a, c - 1);
            else
                return a;
        }


        // Approach that uses a loop is simplier.
        static int Foo2(int a, int b, int c)
        {
            var sum = 0;
	        for (int i = 1; i < c-1; i++)
	        {
		       sum = a + b;
		       a = b;
		       b = sum;
            }

            return sum;
        }

        /* 
         * This method represents a simple sorting algorithm, so known as "bubble" sort.
         * It repeatedly steps through the list, compares adjacent elements and swaps them if they are in the wrong order
         * Advantages
              - good for education as it the simplest one.
         * Disadvantages
         *    - it is slow.
         */
        static int[] Bar(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
                for (int j = 0; j < arr.Length - 1; j++)
                    if (arr[j] > arr[j + 1])
                    {
                        int t = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = t;
                    }
            return arr;
        }

        // Insertion sort algorithm which is way better than "bubble" sort.
        static int[] Bar2(int[] arr)
        {
            int i, key, j;  
            for (i = 1; i < arr.Length; i++) 
            {  
                key = arr[i];  
                j = i - 1;

		        while (j >= 0 && arr[j] > key)
		        {
			        arr[j + 1] = arr[j];
			        j = j - 1;
		        }
		        arr[j + 1] = key;
	        }
	
	        return arr;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Foo result: {0}", Foo(7, 2, 8));
            Console.WriteLine("Foo2 result: {0}", Foo2(7, 2, 8));

            Console.WriteLine("Bar result: {0}", string.Join(", ", Bar(new[] { 7, 2, 8 })));
            Console.WriteLine("Bar2 result: {0}", string.Join(", ", Bar2(new[] { 7, 2, 8 })));

            Console.ReadKey();
        }
    }
}
