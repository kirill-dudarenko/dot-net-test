﻿using log4net;
using System;
using System.Diagnostics;
using System.ServiceModel.Dispatcher;

namespace TG.Exam.Refactoring
{
    public class LoggingOperationInvoker : IOperationInvoker
    {
        IOperationInvoker _baseInvoker;
        ILog _logger;
        string _operationName;

        public LoggingOperationInvoker(IOperationInvoker baseInvoker, DispatchOperation operation, ILog logger)
        {
            _baseInvoker = baseInvoker;
            _operationName = operation.Name;
            _logger = logger;
        }

        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            _logger.InfoFormat(_operationName + " Begins - {0} ", DateTime.Now);

            try
            {
                return _baseInvoker.Invoke(instance, inputs, out outputs);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                outputs = null;
            }
            finally
            {
                stopWatch.Stop();
                _logger.InfoFormat(_operationName + " Elapsed - {0} ", stopWatch.Elapsed);
            }

            return null;
        }

        public bool IsSynchronous => _baseInvoker.IsSynchronous;

        public object[] AllocateInputs()
        {
            return _baseInvoker.AllocateInputs();
        }

        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            return _baseInvoker.InvokeBegin(instance, inputs, callback, state);
        }

        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            return _baseInvoker.InvokeEnd(instance, out outputs, result);
        }
    }
}