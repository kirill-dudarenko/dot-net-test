﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using log4net;
using log4net.Config;

namespace TG.Exam.Refactoring
{
    // uncomment if cache needs to be shared across all users
    //[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    [LoggingServiceBehavior()]
    public class OrderService : IOrderService
    {
        // if cache is a synchronization object, it should not change once created
        private readonly IDictionary<string, Order> cache = new Dictionary<string, Order>();
        private Order GetCachedOrder(string orderId)
        {
            lock (cache)
            {
                if (cache.ContainsKey(orderId))
                {
                    return cache[orderId];
                }

                return null;
            }
        }
        private void CacheOrder(Order order)
        {
            lock (cache)
            {
                var orderId = order.OrderId.ToString();
                if (!cache.ContainsKey(orderId))
                    cache[orderId] = order;
            }
        }
        public Order LoadOrder(string orderId)
        {
            Debug.Assert(!string.IsNullOrEmpty(orderId));
            try
            {
                // there is no need to search order in db if incoming value is not represent valid integer.
                // fast fail/fast exit.
                int intOrderId;
                if (!int.TryParse(orderId, out intOrderId))
                {
                    return null;
                }

                var dbOrder = GetCachedOrder(orderId);

                if (dbOrder != null)
                    return dbOrder;

                var dal = new OrderRepository();
                dbOrder = dal.GetOrder(intOrderId);

                CacheOrder(dbOrder);

                return dbOrder;
            }
            // mask SqlException
            catch (SqlException ex)
            {  
                throw new ApplicationException($"Error: {ex.Message}"); 
            }
        }
    }
}
