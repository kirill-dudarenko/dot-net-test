﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TG.Exam.Refactoring
{
    public class RepositoryBase
    {
        protected readonly string _connectionString = ConfigurationManager.ConnectionStrings["OrdersDBConnectionString"].ConnectionString;

        protected SqlConnection GetConnection()
        {
            var con = new SqlConnection(_connectionString);

            con.Open();

            return con;
        }
    }

    public class OrderRepository : RepositoryBase
    {
        public Order GetOrder(int orderId)
        {
            using (var connection = GetConnection())
            {
                string query = @"
                SELECT OrderId, OrderCustomerId, OrderDate
                FROM dbo.Orders where OrderId=@orderId";

                using (var command = new SqlCommand(query, connection))
                {
                    var parameter = new SqlParameter("@orderId", System.Data.SqlDbType.Int);
                    parameter.Value = orderId;
                    command.Parameters.Add(parameter);

                    using (var reader = command.ExecuteReader())
                    {
                        Order dbOrder = null;
                        if (reader.Read())
                        {
                            dbOrder = new Order
                            {
                                OrderId = (int)reader[0],
                                OrderCustomerId = (int)reader[1],
                                OrderDate = (DateTime)reader[2]
                            };
                        }

                        return dbOrder;
                    }
                }
            }
        }
    }
}