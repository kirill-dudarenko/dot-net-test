﻿using log4net;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace TG.Exam.Refactoring
{
    public class LoggingOperationBehavior : IOperationBehavior
    {
        ILog _logger;

        public LoggingOperationBehavior(ILog logger)
        {
            _logger = logger;
        }

        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
            dispatchOperation.Invoker = new LoggingOperationInvoker(dispatchOperation.Invoker, dispatchOperation, _logger);
        }

        
        public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
        {
            
        }

        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
            
        }

        public void Validate(OperationDescription operationDescription)
        {
            
        }
    }
}